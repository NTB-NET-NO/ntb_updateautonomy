Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib
Imports System.Data.SqlClient

Module Module1

    Private sqlConnectionString As String
    Private errFilePath As String
    Private logFilePath As String
    Private intAutonomyArchiveNtbFolder As Integer
    Private intAutonomyArchiveMaingroup As Integer
    Private intAutonomyArchiveSubgroup As Integer
    Private bAutonomyArchiveEnable As Boolean
    Private AutonomyIdxArchivePath As String
    Private fileIdx As AutonomyIDX = New AutonomyIDX()

    Sub Main()
        Dim intArticleId As Integer = Val(Command())
        Dim strXmlDoc As String
        Dim cn As SqlConnection = New SqlConnection()
        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand
        Dim strIdxFile As String

        Try
            InitConfigValues()
        Catch err As Exception
            LogFile.WriteErr(errFilePath, "ERROR Reading Config values: ", err)
            Return
        End Try

        Try
            cn.ConnectionString = sqlConnectionString
            cn.Open()

            mySqlCommand = New SqlCommand("getXML " & intArticleId, cn)
            myDataReader = mySqlCommand.ExecuteReader()

            ' Get strXmlDoc from Database 
            myDataReader.Read()
            strXmlDoc = myDataReader.GetString(0)
            ' Close when done reading.
            myDataReader.Close()
            cn.Close()
        Catch err As Exception
            LogFile.WriteErr(errFilePath, "ERROR Readin from SQL-server: " & intArticleId, err)
            Return
        End Try

        If strXmlDoc = "" Then
            Dim err As Exception
            LogFile.WriteErr(errFilePath, "ERROR: No articel XML found: " & intArticleId, err)
            Return
        End If

        Try
            ' Autonomy IDX-file generation for Archive search. Only for files Imported to SQL-server!!
            'bNtbFolder = intNtbFolderID And intAutonomyArchiveNtbFolder
            'bMaingroup = intMaingroup And intAutonomyArchiveMaingroup
            'bSubgroup = intSubgroup And intAutonomyArchiveSubgroup
            'If bAutonomyArchiveEnable And (intUrgency > 3) And bMaingroup And bSubgroup And bNtbFolder Then
            ' Only do Autonomy IDX file creation acording to bAutonomyImportEnable AND Bitwise selection of groups etc.
            ' No Autonomy IDX file creation for Urgent messages 
            '   (since they have only very short message, and will often give odd content matching).
            Dim strFilename = "NTB_" & intArticleId.ToString & ".idx"
            strIdxFile = AutonomyIdxArchivePath & "/" & strFilename
            fileIdx.Open(strIdxFile)
            fileIdx.MakeIdx("NTB", intArticleId, strXmlDoc)
            fileIdx.Close()
            LogFile.WriteLog(logFilePath, strIdxFile & ": Written")

            'End If
        Catch err As Exception
            LogFile.WriteErr(errFilePath, "ERROR updating:" & strIdxFile, err)
        End Try

    End Sub

    Private Sub InitConfigValues()
        ' Reading Config values from "NTB_Import.exe.config" file

        sqlConnectionString = AppSettings("SqlConnectionString")
        logFilePath = AppSettings("logFilePath")
        errFilePath = AppSettings("errFilePath")

        If Not Directory.Exists(logFilePath) Then
            Directory.CreateDirectory(logFilePath)
        End If

        If Not Directory.Exists(errFilePath) Then
            Directory.CreateDirectory(errFilePath)
        End If

        ' Get Autonomy IDX File destination path
        AutonomyIdxArchivePath = AppSettings("AutonomyIdxArchivePath")
        If Not Directory.Exists(AutonomyIdxArchivePath) Then
            Directory.CreateDirectory(AutonomyIdxArchivePath)
        End If


        ' Boolean variable for choosing to make Autonomy import files or not
        bAutonomyArchiveEnable = (LCase(AppSettings("AutonomyArchiveEnable")) = "true") Or (LCase(AppSettings("AutonomyArchiveEnable")) = "1") Or (LCase(AppSettings("AutonomyArchiveEnable")) = "yes")
        ' Bit pattern variables for choosing to make Autonomy import files or not
        intAutonomyArchiveNtbFolder = AppSettings("AutonomyArchiveNtbFolder")
        intAutonomyArchiveMaingroup = AppSettings("AutonomyArchiveMaingroup")
        intAutonomyArchiveSubgroup = AppSettings("AutonomyArchiveSubgroup")

    End Sub

End Module
